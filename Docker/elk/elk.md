# logstash 配置
```yaml
input {   
    tcp {
        port => "5000"
        type => syslog
        codec => json_lines
    }  
}
output {
    elasticsearch {
        hosts => ["10.11.3.12:9200"]
        index => "logs-%{+YYYY.MM.dd}"
    }   
}
```
# elasticsearch 查询
```bash

```
# kibana 配置

# sentinl报警配置
```yaml

```
# alerter报警配置
```yaml

```