# 调仓与转换

# 交易规则

# 回测

# 交易规则

# 术语
[投资指导术语](http://zhidao.agutong.com/entries)

[多空指标 （ BBI指标 、 BBI ）](http://zhidao.agutong.com/entries/57348005a02fb4000f89358b)





成交量：
成交额：
买1~买5量：
买1~买5价：
卖1~卖5量：
卖1~卖5价：

MACD：
金叉：
死叉：

KDJ：

```
知乎：
作者：文戈戈
链接：https://www.zhihu.com/question/23173321/answer/33932592
来源：知乎
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。

开盘价：指每天成交中最先的一笔成交的价格。收盘价：指每天成交中最后的一笔股票的价格，也就是收盘价格。 
成交数量：指当天成交的股票数量。最高价：指当天股票成交的各种不同价格是最高的成交价格。最低价：指当天成交的不同价格中最低成交价格。 
升高盘：是指开盘价比前一天收盘价高出许多。 
开低盘：是指开盘价比前一天收盘价低出许多。 
盘档：是指投资者不积极买卖，多采取观望态度，使当天股价的变动幅度很小，这种情况称为盘档。 
整理：是指股价经过一段急剧上涨或下跌后，开始小幅度波动，进入稳定变动阶段，这种现象称为整理，整理是下一次大变动的准备阶段。盘坚：股价缓慢上涨，称为盘坚。 
盘软：股价缓慢下跌，称为盘软。 
跳空：指受强烈利多或利空消息刺激，股价开始大幅度跳动。跳空通常在股价大变动的开始或结束前出现。 
回档：是指股价上升过程中，因上涨过速而暂时回跌的现象。 
反弹：是指在下跌的行情中，股价有时由于下跌速度太快，受到买方支撑面暂时回升的现象。反弹幅度较下跌幅度小，反弹后恢复下跌趋势。 
成交笔数：是指当天各种股票交易的次数。成交额：是指当天每种股票成交的价格总额。 
最后喊进价：是指当天收盘后，买者欲买进的价格。 
最后喊出价：是指当天收盘后，卖者的要价。多头：对股票后市看好，先行买进股票，等股价涨至某个价位，卖出股票赚取差价的人。 
空头：是指变为股价已上涨到了最高点，很快便会下跌，或当股票已开始下跌时，变为还会继续下跌，趁高价时卖出的投资者。 
涨跌：以每天的收盘价与前一天的收盘价相比较，来决定股票价格是涨还是跌。一般在交易台上方的公告牌上用“+”“-”号表示。 
价位：指喊价的升降单位。价位的高低随股票的每股市价的不同而异。以上海证券交易所为例：每股市价末满100元 价位是0.10元；每股市价100-200元 价位是0.20元；每股市价200-300元 价位是0.30元；每股市价300-400元 价位是0.50元；每墒屑?00元以上 价位是1.00元； 
僵牢：指股市上经常会出现股价徘徊缓滞的局面，在一定时期内既上不去，也下不来，上海投资者们称此为僵牢。 
配股：公司发行新股时，按股东所有人参份数，以特价（低于市价）分配给股东认购。 
要价、报价：股票交易中卖方愿出售股票的最低价格。 
行情牌： 一些大银行和经纪公司，证券交易所设置的大型电子屏幕，可随时向客户提供股票行情。 
盈亏临界点：交易所股票交易量的基数点，超过这一点就会实现盈利，反之则亏损。 
填息：除息前，股票市场价格大约等于没有宣布除息前的市场价格加将分派的股息。因而在宣布除息后股价将上涨。除息完成后，股价往往会下降到低于除息前的股价。二者之差约等于股息。如果除息完成后，股价上涨接近或超过除息前的股价，二者的差额被弥补，就叫填息。 
票面价值：指公司最初所定股票票面值。 
法定资本：例如一家公司的法定资本是2000万元，但开业时只需1000万元便足够，持股人缴足1000万元便是缴足资本。 
蓝筹股：指资本雄厚，信誉优良的挂牌公司发行的股票。 
信托股：指公积金局批准公积金持有人可投资的股票。 
可进行按金交易股：指可以利用按金买卖的股票。 
包括股息：买卖股票时包括股息在内。 
不包括股息：买卖股票时不包括股息在内。  包括红股：买卖股票时包括公司发放红股在内。 
不包括红股：买卖股票时不包括红股。包括附加股：可享有公司分发的附加股。  不包括附加股：不连附加股在内。 
包括一切权益：包括股息、红股或股加股等各种权益在内。 
不包括一切权益：即不享有各种权益。  经纪人佣金：经纪人执行客户的指令所收取的报酬，通常以成交金额的百分比计算。多头市场：也称牛市，就是股票价格普遍上涨的市场。 
空头市场：股价呈长期下降趋势的市场，空头市场中，股价的变动情况是大跌小涨。亦称熊市。 
股本：所有代表企业所有权的股票，包括普通股和优先股。资本化证券：根据普通股股东持股股份的比例，免费提供的新股，亦称临时股或红利股。 
现卖：证券交易所中交易成交后，要求在当天交付证券的行为称为现卖。 
多翻空：原本看好行情的多头，看法改变，不但卖出手中的股票，还借股票卖出，这种行为称为翻空或多翻空。 
空翻多：原本作空头者，改变看法，不但把卖出的股票买回，还买进更多的股票，这种行为称为空翻多。买空：预计股价将上涨，因而买入股票，在实际交割前，再将买入的股票卖掉，实际交割时收取差价或补足差价的一种投机行为。 
卖空：预计股价将下跌，因而卖出股票，在发生实际交割前，将卖出股票如数补进，交割时，只结清差价的投机行为。 
利空：促使股价下跌，以空头有利的因素和消息。 
利多：是刺激股价上涨，对多头有利的因素和消息。长空：是对股价前景看坏，借来股票卖，或卖出股票期货，等相当长一段时间后才买回的行为。 
短空：变为股价短期内看跌，借来股票卖出，短时间内即补回的行为。 
长多：是对股价远期看好，认为股价会长期不断上涨，因而买进股票长期持有，等股价上涨相当长时间后再卖出，赚取差价收益的行为。 
短多：是对股价短期内看好，买进股票，如果股价略有不涨即卖出的行为。补空：是空关买回以前卖出的股票的行为。 
吊空：是指抢空头帽子，卖空股票，不料当天股价末下跌，只好高价赔钱补进。 
多杀多：是普遍认为当天股价将上涨，于是市场上抢多头帽子的特别多，然而股价却没有大幅度上涨，等交易快结束时，竞相卖出，造成收盘价大幅度下跌的情况。轧空：是普遍认为当天股价将下跌，于是都抢空头帽子，然而股价并末大幅度下跌，无法低价买进，收盘前只好竞相补进，反而使收盘价大幅度升高的情况。 
死多：是看好股市前景，买进股票后，如果股价下跌，宁愿放上几年，不嫌钱绝不脱手。 
套牢：是指预期股价上涨，不料买进后，股价一路下跌；或是预期股价下跌，卖出股票后，股价却一路上涨，前者称多头套牢，后者是空头套牢。 
抢帽子：指当天低买再高卖，或高卖再低买，买卖股票的种类和数量都相同，从中赚取差价的行为。 
帽客：从事抢帽子行为的人，称为帽客。断头：是指抢多头帽子，买进股票，不料当天股价末上涨，反而下跌，只好低价赔本卖出。大户：就是大额投资人，例如财团、信托公司以及其它拥有庞大资金的集团或个人。 
散户：就是买卖股票数量很少的小额投资者。 
作手：在股市中炒作哄抬，用不正当方法把股票炒高后卖掉，然后再设法压低行情，低价补回；或趁低价买进，炒作哄抬后，高价卖出。这种人被称为作手。 
吃货：作手在低价时暗中买进股票，叫做吃货。出货：作手在高价时，不动声色地卖出股票，称为出货。掼压：用不正当手段压低股价的行为叫惯压。 
坐轿子：目光锐利或事先得到信息的投资人，在大户暗中买进或卖出时，或在利多或利空消息公布前，先期买进或卖出股票，待散户大量跟进或跟出，造成股价大幅度上涨或下跌时，再卖出或买回，坐享厚利，这就叫“坐轿子” 
抬轿子：利多或利空消息公布后，认为股价将大幅度变动，跟着抢进抢出，获利有限，甚至常被套牢的人，就是给别人抬轿子。 
热门股：是指交易量大、流通性强、价格变动幅度大的股票。 
冷门股：是指交易量小，流通性差甚至没有交易，价格变动小的股票。领导股：是指对股票市场整个行情变化趋势具有领导作用的股票。领导股必为热门股。 
投资股：指发行公司经营稳定，获利能力强，股息高的股票。投机股：指股价因人为因素造成涨跌幅度很大的股票。 
高息股：指发行公司派发较多股息的股票。 
无息股：指发行公司多年末派发股息的股票。 
成长股：指新添的有前途的产业中，利润增长率较高的企业股票。成长股的股价呈不断上涨趋势。
```