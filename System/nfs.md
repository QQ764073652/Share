# ubuntu安装配置nfs
# 安装nfs服务
sudo apt install nfs-kernel-server
# 添加自启动
sudo systemctl enable nfs-blkmap.service
# 添加nfs共享目录为 /data/nfs
cat <<EOF >>/etc/exports
/data/nfs  192.168.1.*(rw,sync,no_root_squash,no_subtree_check,insecure)
EOF

# 重启rpcbind 服务。nfs是一个RPC程序，使用它前，需要映射好端口，通过rpcbind 设定。
/etc/init.d/rpcbind restart
/etc/init.d/nfs-kernel-server restart //重启nfs服务
# 挂载测试是否可用
# sudo mount -t nfs 192.168.1.76:/data/nfs ~/data