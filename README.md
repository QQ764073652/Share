## [运维篇](Share-Server.md)
> 包含：
   >* Docker的使用
   >* Linux常用命令
   >* Nginx、Mysql相关配置 
   >* Git相关
   >* 扩展
    
## [Python篇](Share-Python.md)
> 包含：
   >* Wind数据获取
   >* 爬虫
   >* GUI应用 
   
## [Pandas篇](Share-Pandas.md)
## [Java篇](Share-Server.md)
## [算法篇](Share-Algorithm.md)
## [TensorFlow篇](Share-TensorFlow.md)
## [区块链](Share-BlockChain.md)
> 包含：
   >* 共识算法
   >* 密码学
   >* P2P网络
   >* 比特币
   >* 以太坊
   
   
添加：
    docker image/continer备份恢复
    > docker commit -p [容器id] [备份名]
    > docker save --output [tar name] [image id] 
    
    nginx 反向代理配置，stream代理tcp/udp
    k8s
    
    docker管理和配置
    docker.service地址：   /etc/default/docker