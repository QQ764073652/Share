# 附
### docker
```cmd
> docker --help
Usage:	docker COMMAND

A self-sufficient runtime for containers

Options:
      --config string      Location of client config files (default "/home/ubuntu/.docker")
  -D, --debug              Enable debug mode
      --help               Print usage
  -H, --host list          Daemon socket(s) to connect to (default [])
  -l, --log-level string   Set the logging level ("debug", "info", "warn", "error", "fatal") (default "info")
      --tls                Use TLS; implied by --tlsverify
      --tlscacert string   Trust certs signed only by this CA (default "/home/ubuntu/.docker/ca.pem")
      --tlscert string     Path to TLS certificate file (default "/home/ubuntu/.docker/cert.pem")
      --tlskey string      Path to TLS key file (default "/home/ubuntu/.docker/key.pem")
      --tlsverify          Use TLS and verify the remote
  -v, --version            Print version information and quit

Management Commands:
  container   Manage containers
  image       Manage images
  network     Manage networks
  node        Manage Swarm nodes
  plugin      Manage plugins
  secret      Manage Docker secrets
  service     Manage services
  stack       Manage Docker stacks
  swarm       Manage Swarm
  system      Manage Docker
  volume      Manage volumes

Commands:
  attach      Attach to a running container
  build       Build an image from a Dockerfile
  commit      Create a new image from a container's changes
  cp          Copy files/folders between a container and the local filesystem
  create      Create a new container
  diff        Inspect changes on a container's filesystem
  events      Get real time events from the server
  exec        Run a command in a running container
  export      Export a container's filesystem as a tar archive
  history     Show the history of an image
  images      List images
  import      Import the contents from a tarball to create a filesystem image
  info        Display system-wide information
  inspect     Return low-level information on Docker objects
  kill        Kill one or more running containers
  load        Load an image from a tar archive or STDIN
  login       Log in to a Docker registry
  logout      Log out from a Docker registry
  logs        Fetch the logs of a container
  pause       Pause all processes within one or more containers
  port        List port mappings or a specific mapping for the container
  ps          List containers
  pull        Pull an image or a repository from a registry
  push        Push an image or a repository to a registry
  rename      Rename a container
  restart     Restart one or more containers
  rm          Remove one or more containers
  rmi         Remove one or more images
  run         Run a command in a new container
  save        Save one or more images to a tar archive (streamed to STDOUT by default)
  search      Search the Docker Hub for images
  start       Start one or more stopped containers
  stats       Display a live stream of container(s) resource usage statistics
  stop        Stop one or more running containers
  tag         Create a tag TARGET_IMAGE that refers to SOURCE_IMAGE
  top         Display the running processes of a container
  unpause     Unpause all processes within one or more containers
  update      Update configuration of one or more containers
  version     Show the Docker version information
  wait        Block until one or more containers stop, then print their exit codes

```
### docker-compose

```cmd
> docker-compose -help
Usage:
  docker-compose [-f <arg>...] [options] [COMMAND] [ARGS...]
  docker-compose -h|--help

Options:
  -f, --file FILE             Specify an alternate compose file (default: docker-compose.yml)
  -p, --project-name NAME     Specify an alternate project name (default: directory name)
  --verbose                   Show more output
  -v, --version               Print version and exit
  -H, --host HOST             Daemon socket to connect to

  --tls                       Use TLS; implied by --tlsverify
  --tlscacert CA_PATH         Trust certs signed only by this CA
  --tlscert CLIENT_CERT_PATH  Path to TLS certificate file
  --tlskey TLS_KEY_PATH       Path to TLS key file
  --tlsverify                 Use TLS and verify the remote
  --skip-hostname-check       Don't check the daemon's hostname against the name specified
                              in the client certificate (for example if your docker host
                              is an IP address)

Commands:
  build              Build or rebuild services
  bundle             Generate a Docker bundle from the Compose file
  config             Validate and view the compose file
  create             Create services
  down               Stop and remove containers, networks, images, and volumes
  events             Receive real time events from containers
  exec               Execute a command in a running container
  help               Get help on a command
  kill               Kill containers
  logs               View output from containers
  pause              Pause services
  port               Print the public port for a port binding
  ps                 List containers
  pull               Pulls service images
  push               Push service images
  restart            Restart services
  rm                 Remove stopped containers
  run                Run a one-off command
  scale              Set number of containers for a service
  start              Start services
  stop               Stop services
  unpause            Unpause services
  up                 Create and start containers
  version            Show the Docker-Compose version information

```
### rsync
```cmd
> rsync --help
Usage: rsync [OPTION]... SRC [SRC]... DEST
  or   rsync [OPTION]... SRC [SRC]... [USER@]HOST:DEST
  or   rsync [OPTION]... SRC [SRC]... [USER@]HOST::DEST
  or   rsync [OPTION]... SRC [SRC]... rsync://[USER@]HOST[:PORT]/DEST
  or   rsync [OPTION]... [USER@]HOST:SRC [DEST]
  or   rsync [OPTION]... [USER@]HOST::SRC [DEST]
  or   rsync [OPTION]... rsync://[USER@]HOST[:PORT]/SRC [DEST]
The ':' usages connect via remote shell, while '::' & 'rsync://' usages connect
to an rsync daemon, and require SRC or DEST to start with a module name.

Options
    -v, --verbose       详细模式输出。
    -q, --quiet         精简输出模式。
    -c, --checksum      打开校验开关，强制对文件传输进行校验。
    -a, --archive       归档模式，表示以递归方式传输文件，并保持所有文件属性，等于-rlptgoD。
    -r, --recursive     对子目录以递归模式处理。
    -R, --relative      使用相对路径信息。
    -b, --backup        创建备份，也就是对于目的已经存在有同样的文件名时，将老的文件重新命名为~filename。可以使用--suffix选项来指定不同的备份文件前缀。
    --backup-dir        将备份文件(如~filename)存放在在目录下。
    -suffix=SUFFIX      定义备份文件前缀。
    -u, --update        仅仅进行更新，也就是跳过所有已经存在于DST，并且文件时间晚于要备份的文件，不覆盖更新的文件。
    -l, --links         保留软链结。
    -L, --copy-links    想对待常规文件一样处理软链结。
    --copy-unsafe-links 仅仅拷贝指向SRC路径目录树以外的链结。
    --safe-links        忽略指向SRC路径目录树以外的链结。
    -H, --hard-links    保留硬链结。
    -p, --perms         保持文件权限。
    -o, --owner         保持文件属主信息。
    -g, --group         保持文件属组信息。
    -D, --devices       保持设备文件信息。
    -t, --times         保持文件时间信息。
    -S, --sparse        对稀疏文件进行特殊处理以节省DST的空间。
    -n, --dry-run       现实哪些文件将被传输。
    -w, --whole-file    拷贝文件，不进行增量检测。
    -x, --one-file-system 不要跨越文件系统边界。
    -B, --block-size=SIZE 检验算法使用的块尺寸，默认是700字节。
    -e, --rsh=command   指定使用rsh、ssh方式进行数据同步。
    --rsync-path=PATH   指定远程服务器上的rsync命令所在路径信息。
    -C, --cvs-exclude   使用和CVS一样的方法自动忽略文件，用来排除那些不希望传输的文件。
    --existing          仅仅更新那些已经存在于DST的文件，而不备份那些新创建的文件。
    --delete            删除那些DST中SRC没有的文件。
    --delete-excluded   同样删除接收端那些被该选项指定排除的文件。
    --delete-after      传输结束以后再删除。
    --ignore-errors     及时出现IO错误也进行删除。
    --max-delete=NUM    最多删除NUM个文件。
    --partial           保留那些因故没有完全传输的文件，以是加快随后的再次传输。
    --force             强制删除目录，即使不为空。
    --numeric-ids       不将数字的用户和组id匹配为用户名和组名。
    --timeout=time      ip超时时间，单位为秒。
    -I, --ignore-times  不跳过那些有同样的时间和长度的文件。
    --size-only         当决定是否要备份文件时，仅仅察看文件大小而不考虑文件时间。
    --modify-window=NUM 决定文件是否时间相同时使用的时间戳窗口，默认为0。
    -T --temp-dir=DIR   在DIR中创建临时文件。
    --compare-dest=DIR  同样比较DIR中的文件来决定是否需要备份。
    -P                  等同于 --partial。
    --progress          显示备份过程。
    -z, --compress      对备份的文件在传输时进行压缩处理。
    --exclude=PATTERN   指定排除不需要传输的文件模式。
    --include=PATTERN   指定不排除而需要传输的文件模式。
    --exclude-from=FILE 排除FILE中指定模式的文件。
    --include-from=FILE 不排除FILE指定模式匹配的文件。
    --version           打印版本信息。
    --address           绑定到特定的地址。
    --config=FILE       指定其他的配置文件，不使用默认的rsyncd.conf文件。
    --port=PORT         指定其他的rsync服务端口。
    --blocking-io       对远程shell使用阻塞IO。
    -stats              给出某些文件的传输状态。
    --progress          在传输时现实传输过程。
    --log-format=formAT 指定日志文件格式。
    --password-file=FILE 从FILE中得到密码。
    --bwlimit=KBPS      限制I/O带宽，KBytes per second。
    -h, --help          显示帮助信息。
```
### netstat
```cmd
> netstat --help
usage: netstat [-vWeenNcCF] [<Af>] -r         netstat {-V|--version|-h|--help}
       netstat [-vWnNcaeol] [<Socket> ...]
       netstat { [-vWeenNac] -i | [-cWnNe] -M | -s }

        -r, --route              display routing table
        -i, --interfaces         display interface table
        -g, --groups             display multicast group memberships
        -s, --statistics         display networking statistics (like SNMP)
        -M, --masquerade         display masqueraded connections

        -v, --verbose            be verbose
        -W, --wide               don't truncate IP addresses
        -n, --numeric            don't resolve names
        --numeric-hosts          don't resolve host names
        --numeric-ports          don't resolve port names
        --numeric-users          don't resolve user names
        -N, --symbolic           resolve hardware names
        -e, --extend             display other/more information
        -p, --programs           display PID/Program name for sockets
        -c, --continuous         continuous listing

        -l, --listening          display listening server sockets
        -a, --all, --listening   display all sockets (default: connected)
        -o, --timers             display timers
        -F, --fib                display Forwarding Information Base (default)
        -C, --cache              display routing cache instead of FIB

```
### sed
```cmd
Usage: sed [OPTION]... {script-only-if-no-other-script} [input-file]...

  -n, --quiet, --silent
                 suppress automatic printing of pattern space
  -e script, --expression=script
                 add the script to the commands to be executed
  -f script-file, --file=script-file
                 add the contents of script-file to the commands to be executed
  --follow-symlinks
                 follow symlinks when processing in place
  -i[SUFFIX], --in-place[=SUFFIX]
                 edit files in place (makes backup if SUFFIX supplied)
  -l N, --line-length=N
                 specify the desired line-wrap length for the `l' command
  --posix
                 disable all GNU extensions.
  -r, --regexp-extended
                 use extended regular expressions in the script.
  -s, --separate
                 consider files as separate rather than as a single continuous
                 long stream.
  -u, --unbuffered
                 load minimal amounts of data from the input files and flush
                 the output buffers more often
  -z, --null-data
                 separate lines by NUL characters
      --help     display this help and exit
      --version  output version information and exit
a\      在当前行下面插入文本。
i\      在当前行上面插入文本。
c\      把选定的行改为新的文本。
d       删除，删除选择的行。
D       删除模板块的第一行。
s       替换指定字符
h       拷贝模板块的内容到内存中的缓冲区。
H       追加模板块的内容到内存中的缓冲区。
g       获得内存缓冲区的内容，并替代当前模板块中的文本。
G       获得内存缓冲区的内容，并追加到当前模板块文本的后面。
l       列表不能打印字符的清单。
n       读取下一个输入行，用下一个命令处理新的行而不是用第一个命令。
N       追加下一个输入行到模板块后面并在二者间嵌入一个新行，改变当前行号码。
p       打印模板块的行。
P       (大写) 打印模板块的第一行。
q       退出Sed。
b lable 分支到脚本中带有标记的地方，如果分支不存在则分支到脚本的末尾。
r file  从file中读行。
t label if分支，从最后一行开始，条件一旦满足或者T，t命令，将导致分支到带有标号的命令处，或者到脚本的末尾。
T label 错误分支，从最后一行开始，一旦发生错误或者T，t命令，将导致分支到带有标号的命令处，或者到脚本的末尾。
w file  写并追加模板块到file末尾。  
W file  写并追加模板块的第一行到file末尾。  
!       表示后面的命令对所有没有被选定的行发生作用。  
=       打印当前行号码。  
#       把注释扩展到下一个换行符以前。  

^       匹配行开始，如：/^sed/匹配所有以sed开头的行。
$       匹配行结束，如：/sed$/匹配所有以sed结尾的行。
.       匹配一个非换行符的任意字符，如：/s.d/匹配s后接一个任意字符，最后是d。
*       匹配0个或多个字符，如：/*sed/匹配所有模板是一个或多个空格后紧跟sed的行。
[]      匹配一个指定范围内的字符，如/[ss]ed/匹配sed和Sed。  
[^]     匹配一个不在指定范围内的字符，如：/[^A-RT-Z]ed/匹配不包含A-R和T-Z的一个字母开头，紧跟ed的行。
\(..\)  匹配子串，保存匹配的字符，如s/\(love\)able/\1rs，loveable被替换成lovers。
&       保存搜索字符用来替换其他字符，如s/love/**&**/，love这成**love**。
\<      匹配单词的开始，如:/\<love/匹配包含以love开头的单词的行。
\>      匹配单词的结束，如/love\>/匹配包含以love结尾的单词的行。
x\{m\}  重复字符x，m次，如：/0\{5\}/匹配包含5个0的行。
x\{m,\} 重复字符x，至少m次，如：/0\{5,\}/匹配至少有5个0的行。
x\{m,n\} 重复字符x，至少m次，不多于n次，如：/0\{5,10\}/匹配5~10个0的行。
```
