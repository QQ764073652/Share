#!/bin/bash
# 科大环境自动化部署脚本

basedir=$(
    cd $(dirname $0)
    pwd
)

cd ${basedir}/openCommunity
if [[ ! -d "${basedir}/openCommunity" ]]; then
    git clone http://202.38.69.240:10070/leinao-application/openCommunity.git
fi
# 全局缓存用户、密码
cd ${basedir}/openCommunity
git checkout dev
git pull

rm -rf ${basedir}/openCommunity_work
cp -r ${basedir}/openCommunity ${basedir}/openCommunity_work

PROJECT_PATH=${basedir}/openCommunity_work
echo "WorkDir:"${PROJECT_PATH}
# db conf
for line in $(find ${PROJECT_PATH} | grep 'db.properties'); do
    cat <<EOF >${line}
jdbc.driver=com.mysql.jdbc.Driver
jdbc.url=jdbc:mysql://leinao-database:3306/cnbita?characterEncoding=utf-8
jdbc.username=root
jdbc.password=P@ssw0rd
EOF
done
# 修改zookeeper和dubbo配置(在源码里修改还是在webapps里面修改)
for line in $(find ${PROJECT_PATH} | grep -E 'applicationContext-service.xml|springmvc.xml'); do
    sed -i 's/protocol="zookeeper" address="127.0.0.1:2181"/protocol="zookeeper" address="leinao-zk:2181"/' ${line}
done
# 修改log4j文件
for line in $(find ${PROJECT_PATH} | grep 'log4j.properties'); do
    cat <<EOF >$line
log4j.rootLogger=debug,stdout,logstash
log4j.appender.stdout=org.apache.log4j.ConsoleAppender
log4j.appender.stdout.layout=org.apache.log4j.PatternLayout
log4j.appender.stdout.layout.ConversionPattern=%d %p [%c] - %m%n
log4j.appender.RollingLog.layout=net.logstash.log4j.JSONEventLayoutV1
log4j.appender.RollingLog=org.apache.log4j.DailyRollingFileAppender
log4j.appender.RollingLog.Threshold=TRACE
log4j.appender.RollingLog.File=catalina.out
log4j.appender.RollingLog.DatePattern=.yyyy-MM-dd
# logstash
log4j.appender.logstash=org.apache.log4j.net.SocketAppender
log4j.appender.logstash.port=5000
log4j.appender.logstash.remoteHost=10.11.3.12
#log4j.appender.logstash.Encoding=UTF-8
log4j.appender.logstash.Threshold=DEBUG
log4j.appender.logstash.ReconnectionDelay=60000
log4j.appender.logstash.LocationInfo=true
EOF
done

# 编译项目
cd ${PROJECT_PATH}/leinao-parent && mvn clean install -Dmaven.test.skip=true
cd ${PROJECT_PATH}/leinao-comm && mvn clean install -Dmaven.test.skip=true
cd ${PROJECT_PATH}/leinao-tag && mvn clean install -Dmaven.test.skip=true
cd ${PROJECT_PATH}/leinao-activity && mvn clean install -Dmaven.test.skip=true
cd ${PROJECT_PATH}/leinao-aimarket && mvn clean install -Dmaven.test.skip=true
cd ${PROJECT_PATH}/leinao-communicate && mvn clean install -Dmaven.test.skip=true
cd ${PROJECT_PATH}/leinao-article && mvn clean install -Dmaven.test.skip=true
cd ${PROJECT_PATH}/leinao-manage && mvn clean install -Dmaven.test.skip=true
cd ${PROJECT_PATH}/leinao-controller && mvn clean install -Dmaven.test.skip=true
cd ${PROJECT_PATH}/leinao-operation-web && mvn clean install -Dmaven.test.skip=true
# deploy
sudo rm -rf /data/leinao/community/app-manager-service/webapps/*
sudo mv ${PROJECT_PATH}/leinao-manage/leinao-manage-service/target/leinao-manage-service.war /data/leinao/community/app-manager-service/webapps/

# restart
sudo docker-compose -f dev-community-compose.yml restart