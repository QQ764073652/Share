#!/usr/bin/env bash
while [ 1 ]; do
    tmplog="access.`date +'%s'`.log"
    python ./sample_web_log.py > ./streaming/${tmplog}
    echo "`date +"%F %T"` generating $tmplog succeed"
    sleep 1
done