# 系统：ubuntu 16.04
# 配置daemon.json 位置： /etc/docker/daemon.json
```yaml
{
    "registry-mirrors": ["http://192.168.1.76","https://7bezldxe.mirror.aliyuncs.com/","https://registry.docker--cn.com","https://reg-mirror.qiniu.com"],
    "insecure-registries": ["192.168.1.76"],
    "dns": ["8.8.8.8","223.5.5.5","192.168.1.76"],
    "storage-driver": "overlay2",
    "storage-opts": ["overlay2.override_kernel_check=true"],
    "live-restore": true
}
```
# docker.service配置 位置：/etc/default/docker
```yaml
# Docker Upstart and SysVinit configuration file

#
# THIS FILE DOES NOT APPLY TO SYSTEMD
#
#   Please see the documentation for "systemd drop-ins":
#   https://docs.docker.com/engine/admin/systemd/
#

# Customize location of Docker binary (especially for development testing).
#DOCKERD="/usr/local/bin/dockerd"

# Use DOCKER_OPTS to modify the daemon startup options.
DOCKER_OPTS="--dns 8.8.8.8 --dns 8.8.4.4 --registry-mirror=192.168.1.76"

# If you need Docker to use an HTTP proxy, it can also be specified here.
#export http_proxy="http://127.0.0.1:3128/"

# This is also a handy place to tweak where Docker's temporary files go.
#export DOCKER_TMPDIR="/mnt/bigdrive/docker-tmp"
```

# rancher docker 清除脚本
```bash
docker rm -f $(docker ps -qa)
docker volume rm $(docker volume ls -q)
for mount in $(mount | grep tmpfs | grep '/var/lib/kubelet' | awk '{ print $3 }') /var/lib/kubelet /var/lib/rancher; do umount $mount; done
rm -rf /etc/ceph \
     /etc/cni \
     /etc/kubernetes \
     /opt/cni \
     /opt/rke \
     /run/secrets/kubernetes.io \
     /run/calico \
     /run/flannel \
     /var/lib/calico \
     /var/lib/etcd \
     /var/lib/cni \
     /var/lib/kubelet \
     /var/lib/rancher \
     /var/log/containers \
     /var/log/pods \
     /var/run/calico
```