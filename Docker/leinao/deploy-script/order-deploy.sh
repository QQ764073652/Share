#!/bin/bash
IP='10.11.3.9,10.11.3.10'
USER='root'

basedir=$(
    cd $(dirname $0)
    pwd
)
# 正式环境 （单次clone和checkout）
if [[ ! -d "${basedir}/ordersystem" ]];then
    git clone http://202.38.69.240:10070/leinao-application/ordersystem.git
fi
# 全局缓存用户、密码
cd ${basedir}/ordersystem
git checkout dev
git pull

rm -rf ${basedir}/ordersystem_work
cp -r ${basedir}/ordersystem ${basedir}/ordersystem_work

PROJECT_PATH=${basedir}/ordersystem_work

# 修改db配置文件
for line in $(find ${PROJECT_PATH} | grep 'db.properties'); do
    cat <<EOF >${line}
jdbc.driver=com.mysql.jdbc.Driver
jdbc.url=jdbc:mysql://leinao-database:3306/cnbita?characterEncoding=utf-8
jdbc.username=root
jdbc.password=P@ssw0rd
EOF
done

# 修改zookeeper和dubbo配置(在源码里修改还是在webapps里面修改)
for line in $(find ${PROJECT_PATH} | grep -E 'applicationContext-service.xml|springmvc.xml'); do
    sed -i 's/protocol="zookeeper" address="127.0.0.1:2181"/protocol="zookeeper" address="leinao-zk:2181"/' ${line}
done

# todo dubbo
for line in $(find ${PROJECT_PATH} | grep 'applicationContext-service.xml' | grep 'leinao-order'); do
    port=$(cat $line | grep -Po "port=\"\d+" | awk -F'"' '{print $NF}')
    sed -i 's/name="dubbo" port="'${port}'"/name="dubbo" port="'${port}'"/' ${line}
done

# log4j
for line in $(find ${PROJECT_PATH} | grep 'log4j.properties'); do
    cat <<EOF >$line
log4j.rootLogger=debug,stdout,logstash
log4j.appender.stdout=org.apache.log4j.ConsoleAppender
log4j.appender.stdout.layout=org.apache.log4j.PatternLayout
log4j.appender.stdout.layout.ConversionPattern=%d %p [%c] - %m%n
log4j.appender.RollingLog.layout=net.logstash.log4j.JSONEventLayoutV1
log4j.appender.RollingLog=org.apache.log4j.DailyRollingFileAppender
log4j.appender.RollingLog.Threshold=TRACE
log4j.appender.RollingLog.File=catalina.out
log4j.appender.RollingLog.DatePattern=.yyyy-MM-dd
# logstash
log4j.appender.logstash=org.apache.log4j.net.SocketAppender
log4j.appender.logstash.port=5000
log4j.appender.logstash.remoteHost=10.11.3.12
log4j.appender.logstash.Encoding=UTF-8
log4j.appender.logstash.Threshold=DEBUG
log4j.appender.logstash.ReconnectionDelay=60000
log4j.appender.logstash.LocationInfo=true
EOF
done
# payInfo配置
for line in $(find ${PROJECT_PATH} | grep 'payInfo.properties'); do
    cat <<EOF >${line}
#alipay config
alipay_url = https://service.order.bitahub.com/
alipay_return_url = https://www.bitahub.com/
#weChat config
wechat_url= https://service.order.bitahub.com/
EOF
done
# compiler
cd ${PROJECT_PATH}/leinao-order && mvn clean install -Dmaven.test.skip=true
cd ${PROJECT_PATH}/leinao-controller && mvn clean install -Dmaven.test.skip=true
# deploy
sudo rm -rf /data/leinao/order/app-order-service/webapps/*
sudo rm -rf /data/leinao/order/app-order-controller/webapps/*
sudo mv ${PROJECT_PATH}/leinao-order/leinao-order-service/target/leinao-order-service.war /data/leinao/order/app-order-service/webapps/
sudo mv ${PROJECT_PATH}/leinao-controller/target/leinao-order-controller.war /data/leinao/order/app-order-controller/webapps/ROOT.war
# restart
sudo docker-compose -f dev-order-compose.yml restart