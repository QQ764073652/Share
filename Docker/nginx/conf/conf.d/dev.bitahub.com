 server {
    listen       80;
    listen       443;
    server_name  dev.bitahub.com;

    location / {
        root   /usr/local/nginx/html;
        index  index.html index.htm;
    }
    location /es/ {

        proxy_pass http://10.11.3.12:9200/;
    }
    location /kibana/ {
        #rewrite ^/kibana/(.*)$ /$1 break;
        proxy_pass http://10.11.3.12:5601/;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
        auth_basic  "nginx auth";
        auth_basic_user_file htpasswd;
   }
    location /order/ {
        proxy_pass http://10.11.3.12:8080/;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
   }
    location /rancher/ {
        #rewrite /rancher/(.*) http://10.11.3.12:8888;
        proxy_pass http://10.11.3.12:8888/;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
   }
    location /community-pc/ {
        proxy_pass http://10.11.3.12;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
   }

   location /admin/ {
        proxy_pass http://10.11.3.12;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
   }
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/local/nginx/html;
    }
 }
