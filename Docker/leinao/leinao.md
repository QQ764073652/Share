# 每个compose文件都会产生一个network，如service下会有leinao-tomcat01,leinao-tomcat02，
# 这样会产生一个leinao-defult网络当多个compose时之前应用都是leinao-*，则他们都在一个network里，可以相互访问。
在docker-compose中自定义一个网络，实际创建的网络名称为leinao_test-network(可能是有缓存信息)：
```yaml
version: '2'
services:
  # order-service
  order-service:
    restart: always
    image: tomcat:8.0.36-jre8
    container_name: order-service
    hostname: order-service
    stdin_open: true
    tty: true
    volumes:
      - /data/leinao/order/app-order-service/webapps:/usr/local/tomcat/webapps/
    environment:
      JAVA_OPTS: "-Xms1024m -Xmx1024m -XX:ParallelGCThreads=8 -XX:PermSize=256m -XX:MaxPermSize=512m"
    networks:
      - test-network
networks:
  test-network:
    driver: bridge
```
