#!/usr/bin/env bash
basedir=$(
    cd $(dirname $0)
    pwd
)

cd ${basedir}/openCommunity
if [[ ! -d "${basedir}/openCommunity" ]]; then
    git clone http://202.38.69.240:10070/leinao-application/openCommunity.git
fi
# 全局缓存用户、密码
cd ${basedir}/openCommunity
git checkout dev
git pull

rm -rf ${basedir}/openCommunity_work
cp -r ${basedir}/openCommunity ${basedir}/openCommunity_work

#### pc工程
WEB_PROJECT_PATH=${basedir}/openCommunity_work/leinao-community-site-pc

# 更改配置文件
sed -i "s/var api = '\/leinao-controller\/article\/gpu\/getGpuInfo';/var api = 'http:\/\/leinao-controller:8080\/article\/gpu\/getGpuInfo';/" ${WEB_PROJECT_PATH}/views/activity/gpu/js/index.js
# common.js
sed -i 's/apiUrl = "http:\/\/139.129.203.44:8180";/apiUrl = "http:\/\/leinao-controller:8080";/' ${WEB_PROJECT_PATH}/js/common.js
sed -i 's/shareUrl = "http:\/\/139.129.203.44:8280\/community-pc";/shareUrl = "https:\/\/www.bitahub.com";/' ${WEB_PROJECT_PATH}/js/common.js
# 部署

### mobile工程
MOBILE_PROJECT_PATH=${basedir}/openCommunity_work/leinao-opencommunity-site-mobile
# 更改配置文件
# leinao-community-site-pc/views/activity/gpu/js/index.js
sed -i "s/var api = '\/leinao-controller\/article\/gpu\/getGpuInfo';/var api = 'http:\/\/leinao-controller:8080\/article\/gpu\/getGpuInfo';/" ${WEB_PROJECT_PATH}/views/activity/gpu/js/index.js
# common.js
sed -i 's/https:\/\/service.interface.cnbita.com/http:\/\/leinao-controller:8080/' ${MOBILE_PROJECT_PATH}/js/common.js
sed -i 's/https:\/\/m.cnbita.com/https:\/\/m.bitahub.com/' ${MOBILE_PROJECT_PATH}/js/common.js

#### admin工程
ADMIN_PROJECT_PATH=${basedir}/openCommunity_work/leinao-operation-site
# 更改配置文件
# leinao-community-site-pc/js/common.js
apiUrl = "http://139.129.203.44:8180"
sed -i 's/apiUrl = "http:\/\/139.129.203.44:8180";/apiUrl = "http:\/\/leinao-operation-web:8080";/' ${ADMIN_PROJECT_PATH}/js/common.js
sed -i 's/shareUrl = "http:\/\/139.129.203.44:8180\/community-pc";/shareUrl = "https:\/\/admin.bitahub.com";/' ${ADMIN_PROJECT_PATH}/js/common.js
