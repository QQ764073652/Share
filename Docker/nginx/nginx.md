# 创建nginx
sudo docker run --name nginx-tmp -it -d nginx
# 拷贝配置文件-> /data/nginx/conf
sudo docker cp nginx-tmp:/etc/nginx /data/nginx/conf
# 启动和配置nginx
```yaml
version: '2'
services:
  nginx:
    restart: always
    image: nginx
    container_name: nginx
    hostname: nginx
    stdin_open: true
    tty: true
    ports:
      - "80:80"
      - "443:443"
    volumes:
      - /data/nginx/data:/data/service
      - /data/nginx/conf:/etc/nginx
      - /data/nginx/logs:/data/log/nginx
    command: nginx -g 'daemon off;'
```
# nginx http 反向代理配置
```
server {
    listen       80;
    listen       443;
    server_name  dev.bitahub.com;
    location / {
        if ($request_uri = '/') {
            rewrite ^(.*)$ /views/index.html permanent;
        }
        index  views/index.html;
        error_page 404 https://$host/views/page404.html;
    }
    location /jenkins/ {
        proxy_pass http://10.11.3.12:18080/jenkins/;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
   }
    location /elasticsearch/ {
        proxy_pass http://10.11.3.12:9200/;
    }
    location /kibana/ {
        proxy_pass http://10.11.3.12:5601/;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
        auth_basic  "nginx auth";
        auth_basic_user_file htpasswd;
    }
    location /community-pc/ {
        proxy_pass http://10.11.3.12;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
   }
 }
```
# nginx socket 发现代理配置
```
# 这个要放到nginx.conf中http{}外面，需要一个单独的端口，不能跟http的重复
stream {
    upstream ovpn {
        server 10.11.3.12:1194;
    }
    server {
        listen 1194;
        proxy_connect_timeout 300s;
        proxy_timeout 300s;
        proxy_pass ovpn;
    }
}
```