# 创建volume
OVPN_DATA="ovpn-data"
sudo docker volume create --name $OVPN_DATA
# 指定域名
domain="dev.bitahub.com"
# 初始化配置
sudo docker run -v $OVPN_DATA:/etc/openvpn --log-driver=none --rm kylemanna/openvpn ovpn_genconfig -u udp://${domain}
# 创建证书
sudo docker run -v $OVPN_DATA:/etc/openvpn --log-driver=none --rm -it kylemanna/openvpn ovpn_initpki
# 启动OpenVPN服务器进程
sudo docker run -v $OVPN_DATA:/etc/openvpn -d -p 1194:1194/udp --cap-add=NET_ADMIN kylemanna/openvpn
# 生成没有密码的客户端证书
sudo docker run -v $OVPN_DATA:/etc/openvpn --log-driver=none --rm -it kylemanna/openvpn easyrsa build-client-full CLIENTNAME nopass
# 导出具有嵌入证书的客户端配置
sudo docker run -v $OVPN_DATA:/etc/openvpn --log-driver=none --rm kylemanna/openvpn ovpn_getclient CLIENTNAME > CLIENTNAME.ovpn

# 如果没有公网ip，使用nginx做反向代理，需要stream模块,nginx:latest已包含，原生安装
```bash
cd nginx-*
./configure --prefix=/usr/local/nginx --sbin-path=/usr/local/nginx/sbin/nginx --conf-path=/usr/local/nginx/conf/nginx.conf --with-http_stub_status_module --with-http_gzip_static_module --with-http_ssl_module --with-stream
make
make install
```
# 
```
# 这个要放到nginx.conf中http{}外面，需要一个单独的端口，不能跟http的重复
stream {
    upstream ovpn {
        server 10.11.3.12:1194;
    }
    server {
        listen 1194;
        proxy_connect_timeout 300s;
        proxy_timeout 300s;
        proxy_pass ovpn;
    }
}
```
# mac ovpn软件：Tunnelblick