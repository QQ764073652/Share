# 部署dev-box
```bash
sudo docker run -itd \
        -e COLUMNS=$COLUMNS -e LINES=$LINES -e TERM=$TERM \
        -v /var/run/docker.sock:/var/run/docker.sock \
        -v /pathHadoop:/pathHadoop \
        -v /pathConfiguration:/cluster-configuration  \
        --pid=host \
        --privileged=true \
        --net=host \
        --name=dev-box \
        docker.io/openpai/dev-box
```
# 生成配置文件
```bash
sudo docker exec -it dev-box /bin/bash
git pull 
# 最新代码缺少git依赖
pip install gitpython
cp /pai/deployment/quick-start/quick-start-example.yaml /pai/deployment/quick-start/quick-start.yaml
# 修改配置，添加主机列表
vim /pai/deployment/quick-start/quick-start.yaml
# 生成配置
python paictl.py config generate -i /pai/deployment/quick-start/quick-start.yaml -o ~/pai-config -f
# 部署
python paictl.py cluster k8s-bootup -p ~/pai-config 
```
        
       