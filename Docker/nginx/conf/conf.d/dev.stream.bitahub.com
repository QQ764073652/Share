# 这个要放到nginx.conf中http{}外面，需要一个单独的端口，不能跟htt的重复
stream {
    upstream ovpn {
        server 10.11.3.12:1194;
    }

    server {
        listen 1194;
        proxy_connect_timeout 300s;
        proxy_timeout 300s;
        proxy_pass ovpn;
    }

}
