package com.beta.core.enums;

public interface EnumInterface {

    int getValue();

    String getDescription();
}
