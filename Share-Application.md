# 常用软件
## 文件搜索 - Everything
[下载](http://www.voidtools.com/downloads/)
## 文本编辑 - Notepad++ / Cmd Markdown
[Notepad++下载](https://notepad-plus-plus.org/download/v7.5.5.html)\
[Cmd Markdown](https://www.zybuluo.com/cmd/#)
## 截图 - PicPick
[下载](http://ngwin.com/picpick/download)
## Gif - ScreenToGif
[下载](http://www.screentogif.com/downloads.html?l=zh_cn)
## 音频处理 - Adobe Audition
[下载](https://www.adobe.com/cn/products/audition/free-trial-download.html)
## 压缩软件 - BandZip
[下载](https://www.bandisoft.com/bandizip/cn/)
## SSH - XShell/Xftp
[下载](https://www.netsarang.com/download/main.html)

# 开发套件：
## JetBrains 套件：
### IntelliJ IDEA/JetBrains PyCharm/JetBrains DataGrip
[官网](https://www.jetbrains.com/)
## Redis管理 - RedisDesktopManager
[下载](https://redisdesktop.com/download)
## 版本控制 - Git
[下载](https://git-scm.com/downloads)
## 界面设计 - QtCreator
[下载](https://www.qt.io/download)\
账号：ruanxingbaozi@dingtalk.com
## 数据库 - Navicat Premium
[下载](https://www.jianguoyun.com/p/DdbSfUkQ68fhBhjFkEU)\
账号：ruanxingbaozi@dingtalk.com
## 抓包 - Fiddler/Wireshark
[Fiddler下载](https://www.telerik.com/download/fiddler)\
[Wireshark下载](https://www.wireshark.org/)
## iPhone - 爱思助手 / iCloud
[爱思助手下载](https://www.i4.cn/pros.html)
## P2P网络 - ZeroNet
[下载](https://zeronet.io/)
## 金融 - Wind资讯
[下载](http://www.wind.com.cn/NewSite/wft.html)
## 浏览器 - Chrome
[下载](https://www.google.cn/chrome/index.html)
## 网盘 - 坚果云
[下载](https://www.jianguoyun.com/s/downloads)
## 远控 - TeamViewer / TightVNC
[tightVNC下载](http://www.tightvnc.com/download.php)
## VPN/代理 - OpenVPN / Shadow socksR
[ShadowSocksR下载](https://github.com/shadowsocksrr/shadowsocks-rss)\
[Github-openVpn](https://github.com/OpenVPN/openvpn)\
[openVpn下载](https://openvpn.net/index.php/download/community-downloads.html)
## Http请求 - Postman / curl
[PostMan下载](https://www.getpostman.com/)\
[curl下载](https://curl.haxx.se/download.html)\
[Github-curl](https://github.com/curl/curl)
## 下载 - aria2 / Resilio Sync
[aria2下载](https://www.jianguoyun.com/p/DUz9xE8Q68fhBhjmkEU)
## 笔记 - Evernote
[下载](https://evernote.com/intl/zh-cn/download)
## 词典 - 金山词霸 / Google Translate
[词霸下载](http://cp.iciba.com/)
## PDF - 金山PDF
[下载](http://www.wps.cn/product/kingsoftpdf/)
##文档 - Office
[下载]()
## 窗口句柄获取 - spy++
[下载](https://www.jianguoyun.com/p/DSWP7L8Q68fhBhjykEU)



# Chrome插件
ruanxingbaozi@gmail.com
## XPath Helper
[下载](https://chrome.google.com/webstore/detail/charset/hgimnogjllphhhkhlmebbmlgjoejdpjl)
## Chrono下载管理器 / 添加到aria2
[Chrono下载](https://chrome.google.com/webstore/detail/charset/mciiogijehkdemklbdcbfkefimifhecn)\
[aria2下载](https://chrome.google.com/webstore/detail/charset/nimeojfecmndgolmlmjghjmbpdkhhogl)
## 捕捉网页截图 - FireShot
[下载](https://chrome.google.com/webstore/detail/charset/mcbpblocgmgfnpjjppndjkmgjaogfceg)
## WEB前端助手(FeHelper)
[下载](https://chrome.google.com/webstore/detail/charset/pkgccpejnmalmdinmhkkfafefagiiiad)
## CharSet
[下载](https://chrome.google.com/webstore/detail/charset/oenllhgkiiljibhfagbfogdbchhdchml)
## ElasticSearch Head
[下载](https://chrome.google.com/webstore/detail/charset/ffmkiejjmecolpfloofpjologoblkegm)
## QPush - 从电脑快推文字到手机
[下载](https://chrome.google.com/webstore/detail/charset/eccidpbmllnjfhhnjhaaopeeldnlokbi)
## 印象笔记·剪藏
[下载](https://chrome.google.com/webstore/detail/charset/pioclpoplcdbaefihamjohnefbikjilc)
## 划词翻译
[下载](https://chrome.google.com/webstore/detail/charset/ikhdkkncnoglghljlkmcimlnlhkeamad)
## Scirocco recorder for chrome™ 
[下载](https://chrome.google.com/webstore/detail/charset/ibclajljffeaafooicpmkcjdnkbaoiih)
## Google日历
[下载](https://chrome.google.com/webstore/detail/charset/gmbgaklkmjakoegficnlkhebmhkjfich)
## trello
[下载]()



# 缺少
> 便签 、 计划表




# iPhone App:
## 图像处理：
### 长图 - 截图拼接 / Picsew
### 图像 - 扫描全能王
### 水印 - 快去水印
## 工具 - WorkFlow
## 代理 - Shadowrocket
## 启动器 - AirLaunch
## 流量监控 - DataFlow
## 算法题 - Leetcode
## 数学绘图 - Desmos
## 远控 - Teamview
## GitLab/GitHub - GitBucket
## 笔记 - 印象笔记
## 消息推送 - QPush
## 任务流 - Trello